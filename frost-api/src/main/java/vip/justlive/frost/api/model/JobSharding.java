package vip.justlive.frost.api.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分片
 *
 * @author wubo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobSharding implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * index
   */
  private int index;

  /**
   * 总数
   */
  private int total;

}
